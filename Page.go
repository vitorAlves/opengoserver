package main

type Page struct {
	IDPage      string `json:"id_page"`
	Text        string `json:"text"`
	Background  string `json:"background"`
	Title  string `json:"title"`
	Image       string `json:"image"`
	ParentStory string `json:"parent_story"`
	Deleted     string `json:"deleted"`
}

type Pages []Page
