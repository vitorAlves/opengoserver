package main

import (
	"net/http"

	"github.com/gorilla/mux"
)


type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"GetNews",
		"POST",
		"/",
		GetNews,
	},
	Route{
		"GetPages",
		"POST",
		"/pages/{id_story}",
		GetPages,
	},
	Route{
		"GetAllPages",
		"POST",
		"/pages",
		GetAllPages,
	},
	Route{
		"TodoShow",
		"GET",
		"/todos/{todoId}",
		TodoShow,
	},
}
