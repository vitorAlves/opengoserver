package main

import (
	"encoding/json"
	"fmt"
	_ "os"

	"net/http"
	_ "time"

	"database/sql"
	_ "github.com/go-sql-driver/mysql"

	"github.com/gorilla/mux"
)

func getJSONPages(sqlString string) []byte {
	db, err := sql.Open("mysql", "user:password@tcp(website)/dbName?parseTime=true")
	rows, err := db.Query(sqlString)
	if err != nil {
	}
	defer rows.Close()
	columns, err := rows.Columns()
	if err != nil {
	}

	pages := Pages{}

	count := len(columns)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		page := Page{}

		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		/* str := fmt.Sprintf("%v", values) */
		// enc := json.NewEncoder(os.Stdout)
		/* rowValues := enc.Encode(entry) */

		data, err := json.Marshal(entry)
		if err != nil {
			fmt.Printf("marshal failed: %s", err)
		}

		// fmt.Println("this", str, tableData)
		json.Unmarshal([]byte(data), &page)
		pages = append(pages, page)

	}
	jsonData, err := json.Marshal(pages)
	if err != nil {
	}
	//	fmt.Println(string(jsonData))
	return jsonData
}

func getJSONNews(sqlString string) []byte {
	db, err := sql.Open("mysql", "user:password@tcp(website)/dbName?parseTime=true")
	rows, err := db.Query(sqlString)
	if err != nil {
	}
	defer rows.Close()
	columns, err := rows.Columns()
	if err != nil {
	}
	stories := Stories{}

	count := len(columns)
	values := make([]interface{}, count)
	valuePtrs := make([]interface{}, count)
	for rows.Next() {
		story := Story{}

		for i := 0; i < count; i++ {
			valuePtrs[i] = &values[i]
		}
		rows.Scan(valuePtrs...)
		entry := make(map[string]interface{})
		for i, col := range columns {
			var v interface{}
			val := values[i]
			b, ok := val.([]byte)
			if ok {
				v = string(b)
			} else {
				v = val
			}
			entry[col] = v
		}
		/* str := fmt.Sprintf("%v", values) */
		// enc := json.NewEncoder(os.Stdout)
		/* rowValues := enc.Encode(entry) */

		data, err := json.Marshal(entry)
		if err != nil {
			fmt.Printf("marshal failed: %s", err)
		}

		// fmt.Println("this", str, tableData)
		json.Unmarshal([]byte(data), &story)
		stories = append(stories, story)

	}
	jsonData, err := json.Marshal(stories)
	if err != nil {
	}
	//	fmt.Println(string(jsonData))
	return jsonData
}

// Get all news articles
func GetNews(w http.ResponseWriter, r *http.Request) {

	jsonData := (getJSONNews("SELECT * from stories"))

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)

	w.Write(jsonData)
}

//GetPages from a single news article
func GetPages(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	parent_story := vars["id_story"]
	fmt.Println(parent_story)

	jsonData := (getJSONPages("SELECT * from pages where parent_story=" + parent_story))

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)

	w.Write(jsonData)

}
func GetAllPages(w http.ResponseWriter, r *http.Request) {

	jsonData := (getJSONPages("SELECT * from pages"))

	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(http.StatusOK)

	w.Write(jsonData)

}
func TodoShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	todoId := vars["todoId"]
	fmt.Fprintln(w, "Todo show:", todoId)
}
