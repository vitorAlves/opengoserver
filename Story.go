package main

import "time"

type Story struct {
	IDStory    string       `json:"id_story"`
	Title      string    `json:"title"`
	Date       time.Time `json:"date"`
	Description string    `json:"description"`
	Url string    `json:"url"`
	Background string    `json:"background"`
	Thumbnail  string    `json:"thumbnail"`
	Deleted     string `json:"deleted"`
}

type Stories []Story
